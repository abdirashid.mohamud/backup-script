#!/bin/bash

# Set the source and target directories
SOURCE_DIR="/path/to/source/directory"
TARGET_DIR="/path/to/target/directory"

# Set the remote target, if applicable
REMOTE_TARGET="username@remote.host:/path/to/target/directory"

# Create a timestamped directory name for the backup
BACKUP_DIR="${TARGET_DIR}/$(date +%Y-%m-%d-%H-%M-%S)"

# Create the backup directory if it doesn't already exist
mkdir -p "$BACKUP_DIR"

# Copy the contents of the source directory to the backup directory using rsync
rsync -a --delete --link-dest="$TARGET_DIR/latest" "$SOURCE_DIR" "$BACKUP_DIR"

# Create a symbolic link to the latest backup for future incremental backups
rm -f "$TARGET_DIR/latest"
ln -s "$BACKUP_DIR" "$TARGET_DIR/latest"

# If the target directory is remote, copy the backup to the remote target
if [ -n "$REMOTE_TARGET" ]; then
    rsync -a "$BACKUP_DIR" "$REMOTE_TARGET"
fi
